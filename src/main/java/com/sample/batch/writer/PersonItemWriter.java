package com.sample.batch.writer;

import java.util.List;

import org.springframework.batch.item.ItemWriter;

import com.sample.batch.model.Person;

public class PersonItemWriter implements ItemWriter<Person> {

	@Override
	public void write(List<? extends Person> items) throws Exception {

		items.forEach(System.out::println);
		System.out.println("------------------------------");

	}

}
