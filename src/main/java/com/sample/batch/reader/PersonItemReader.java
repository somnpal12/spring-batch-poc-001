package com.sample.batch.reader;

import java.util.ArrayList;
import java.util.List;

import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import com.sample.batch.model.Person;

public class PersonItemReader implements ItemReader<Person> {

	public List<Person> personList;
	int counter;

	@BeforeStep
	public void init() {
		personList = new ArrayList<>();
		personList.add(new Person("Jill", "Doe"));
		personList.add(new Person("Joe", "Doe"));
		personList.add(new Person("Justin", "Doe"));
		personList.add(new Person("Jane", "Doe"));
		personList.add(new Person("John", "Doe"));
		counter = 0;
	}

	@Override
	public Person read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
		Person p = null;
		if (counter < personList.size()) {
			p = personList.get(counter++);
		}
		
		return p;
	}

}
